alert("Hello, B273");
/* 
//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}
*/


//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects


//Use an object literal
// ENCAPSULATION
//spaghetti code - code that is poorly organized that it becomes almost impossible to wqork with

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade average are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		for(let i=0; i<this.grades.length; i++){
			sum += this.grades[i];
		}
		let average = sum / this.grades.length;
		return average;
	},
	willPass(){
		let average = this.computeAve();
		return average >= 85;
	},
	willPassWithHonors(){
		let average = this.computeAve();
		return average >= 90 ? true : (average >= 85 ? false : undefined);
	  }	  
	/*	willPass(){
		let sum = 0;
		for(let i=0; i<this.grades.length; i++){
			sum += this.grades[i];
		}
		let average = sum / this.grades.length;
		return average >= 85;
	} */
}

//Mini-exercise
	//create function/methof that will get/compute the quarterly average of the studentOne's grades

	//create function willPass() will return if the average is >=85, and false if not

	//create a function called willPassWithHonors() that returns true if the student has passed and their average grade is >=90. the function returns false if either one is met

console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quarterly grade average is  is ${studentOne.grades}`);

/* Quiz
1. What is the term given to unorganized code that's very hard to work
with? Spaghetti code

2. How are object literals written in JS? Comma-separated list of name-value pairs inside of curly braces

3. What do you call the concept of organizing information and
functionality to belong to an object? Object-Oriented Programming (OOP)

4. If studentOne has a method named enroll(), how would you invoke it? studentOne.enroll();

5. True or False: Objects can have objects as properties. True

6. What is the syntax in creating key-value pairs? key-value pairs in an object are created using the colon : to separate the key and value, and the comma , to separate each pair. 

7. True or False: A method can have no parameters and still work. True

8. True or False: Arrays can have objects as elements. True

9. True or False: Arrays are objects. True
10. True or False: Objects can have arrays as properties. True
 */

//Function Coding
//1.

let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],

	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade average are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		for(let i=0; i<this.grades.length; i++){
			sum += this.grades[i];
		}
		let average = sum / this.grades.length;
		return average;
	},
	willPass(){
		let average = this.computeAve();
		return average >= 85;
	},
	willPassWithHonors(){
		let average = this.computeAve();
		return average >= 90 ? true : (average >= 85 ? false : undefined);
	  }	  
}

let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade average are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		for(let i=0; i<this.grades.length; i++){
			sum += this.grades[i];
		}
		let average = sum / this.grades.length;
		return average;
	},
	willPass(){
		let average = this.computeAve();
		return average >= 85;
	},
	willPassWithHonors(){
		let average = this.computeAve();
		return average >= 90 ? true : (average >= 85 ? false : undefined);
	  }	  
}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade average are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		for(let i=0; i<this.grades.length; i++){
			sum += this.grades[i];
		}
		let average = sum / this.grades.length;
		return average;
	},
	willPass(){
		let average = this.computeAve();
		return average >= 85;
	},
	willPassWithHonors(){
		let average = this.computeAve();
		return average >= 90 ? true : (average >= 85 ? false : undefined);
	  }	  
}

//5-9
let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],

	countHonorStudents(){
		let count = 0
		for(let i=0; i<this.students.length; i++){
			if(this.students[i].willPassWithHonors()){
				count++;
			}
		}
		return count		
	},
	honorsPercentage(){
		let total = this.students.length;
		let honorCount = this.countHonorStudents();
		return (honorCount / total) * 100;
	},
	retrieveHonorStudentInfo(){
		let honorStudents = [];
		for(let i=0; i<this.students.length; i++){
			if(this.students[i].willPassWithHonors()){
				let info = {
					email: this.students[i].email,
					averageGrade: this.students[i].computeAve()
				};
				honorStudents.push(info);
			}
		}
		return honorStudents;
	},
	sortHonorStudentsByGradeDesc(){
		let honorStudents = this.retrieveHonorStudentInfo();
		honorStudents.sort((a, b) => b.averageGrade - a.averageGrade);
		return honorStudents;
	}
};